import os
from django.test import TestCase
from django.contrib.auth.models import User
from photo.models import Photo


class LogInTest(TestCase):
    credentials = {}

    def setUp(self):
        self.credentials = {
            'username': 'user1',
            'password': 'secret_password'
        }

        user=User.objects.create_user(**self.credentials)

    def test_login_logout(self):
        response = self.client.post('/accounts/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
        response = self.client.post('/accounts/logout/', self.credentials, follow=True)
        self.assertFalse(response.context['user'].is_active)

    def test_signup(self):
        signup_data = {
            'first_name': 'some_name',
            'last_name': 'some_last_name',
            'username': 'user2',
            'email': 'user@test.pl',
            'password1': 'a1b2c3b4',
            'password2': 'a1b2c3b4',
        }
        response = self.client.post('/signup/', signup_data, follow=True)
        self.assertTrue(response.context['user'].is_active)
        response = self.client.post('/accounts/logout/', self.credentials, follow=True)
        self.assertFalse(response.context['user'].is_active)



class PhotoUploadTest(TestCase):
    TEST_PHOTO = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_data', 'stoat.png')
    credentials = {}

    def setUp(self):
        self.credentials = {
            'username': 'user1',
            'password': 'secret_password'
        }

        User.objects.create_user(**self.credentials)

    def test_upload_photo(self):
        response = self.client.post('/accounts/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
        self.assertEquals(Photo.objects.all().count(), 0)
        with open(self.TEST_PHOTO, 'rb') as f:
            response = self.client.post(
                '/photos/upload/',
                {'description': 'white stoat', 'photo': f},
                follow=True
            )
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Photo.objects.count(), 1)
        self.assertEquals(Photo.objects.get().description, 'white stoat')
        self.assertEquals(Photo.objects.get().owner.username, 'user1')
        self.assertTrue(Photo.objects.get().photogooglevisiondetails_set.exists())

