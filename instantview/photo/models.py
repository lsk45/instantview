from base64 import b64encode
import json
import requests
from django.conf import settings
from django.db import models


class Photo(models.Model):
    photo = models.FileField(upload_to='photos/%Y/%m/%d/')
    description = models.CharField(max_length=255, blank=True, null=False, default='desciption')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE, editable=False)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated_at',)

    def save(self, *args, **kwargs):
        created = self.id is None
        super(Photo, self).save(*args, **kwargs)
        if created:
            object_data = self.get_google_visionapi_data()
            try:
                PhotoGoogleVisionDetails.objects.filter(photo=self).delete()
                for i in object_data['responses'][0]['labelAnnotations']:
                    PhotoGoogleVisionDetails.objects.create(
                        photo=self,
                        description=i['description'],
                        score=i['score']
                    )
            except (IndexError, KeyError, TypeError):
                pass

    def get_google_visionapi_data(self, detection_types=None, max_result=5):

        default_detection_types = ['LABEL_DETECTION']

        dt = default_detection_types if detection_types is None else detection_types
        features = [{'type': i, 'maxResults': max_result} for i in dt]
        self.photo.open()
        image = {'content': b64encode(self.photo.read()).decode('ascii')}
        request = {'requests': [{'image': image, 'features': features}]}
        response = requests.post(settings.GOOGLE_VISION_API_URL, json=request)
        if response.status_code != 200:
            return
        return response.json()


class PhotoGoogleVisionDetails(models.Model):
    photo = models.ForeignKey(Photo, editable=False, null=False, on_delete=models.CASCADE)
    description = models.CharField(max_length=100, null=False, editable=False)
    score = models.DecimalField(max_digits=9, decimal_places=8, editable=False)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-score',)

