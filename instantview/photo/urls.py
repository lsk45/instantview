from django.conf.urls import url

from . import views
from .models import Photo


urlpatterns = [
    url(r'^$', views.PhotoListView.as_view(), name='list'),
    url(r'^upload/$', views.PhotoUploadView.as_view(), name='upload'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.PhotoDeleteView.as_view(), name='delete'),
    url(r'^update/(?P<pk>[0-9]+)/$', views.PhotoUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>[0-9]+)/$', views.PhotoDetailView.as_view(), name='detail'),
]

