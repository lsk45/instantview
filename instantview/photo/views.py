from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, DetailView, ListView,\
    TemplateView, UpdateView

from photo.models import Photo

class UserObjectsMixin:

    def get_queryset(self):
        return Photo.objects.filter(owner=self.request.user)


class PhotoListView(LoginRequiredMixin, UserObjectsMixin, ListView):
    paginate_by = 16 # 4x4 grid


class PhotoDetailView(LoginRequiredMixin, UserObjectsMixin, DetailView): pass


class PhotoUploadView(LoginRequiredMixin, CreateView):
    model = Photo
    fields = 'description', 'photo'

    def get_success_url(self):
        return reverse('photo:detail', kwargs={'pk': self.object.id})

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(PhotoUploadView, self).form_valid(form)


class PhotoUpdateView(LoginRequiredMixin, UserObjectsMixin, UpdateView):
    model = Photo
    fields = 'description', 'photo'

    def get_success_url(self):
        return reverse('photo:detail', kwargs={'pk': self.object.id})


class PhotoDeleteView(LoginRequiredMixin, UserObjectsMixin, DeleteView):

    def get_success_url(self):
        return reverse('photo:list')
