from django.contrib import admin
from photo.models import Photo, PhotoGoogleVisionDetails


admin.site.register(Photo)
admin.site.register(PhotoGoogleVisionDetails)

