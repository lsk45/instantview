from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views

from instantview.views import IndexView, signup


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url('accounts/login/$', auth_views.LoginView.as_view(), name='login'),
    url('accounts/logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^signup/$', signup, name='signup'),
    url(r'^photos/', include('photo.urls', namespace='photo')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

