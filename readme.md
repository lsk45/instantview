Simple web application for photos storage
=========================================

dependencies:

* certifi 2017.11.5
* chardet 3.0.4
* decorator 4.1.2
* Django 1.11
* idna 2.6
* ipython 6.2.1
* ipython-genutils 0.2.0
* jedi 0.11.0
* olefile 0.44
* parso 0.1.0
* pexpect 4.3.0
* pickleshare 0.7.4
* Pillow 4.3.0
* prompt-toolkit 1.0.15
* ptyprocess 0.5.2
* Pygments 2.2.0
* pytz 2017.3
* requests 2.18.4
* simplegeneric 0.8.1
* six 1.11.0
* traitlets 4.3.2
* urllib3 1.22
* wcwidth 0.1.7

### install:

```
pip install -r requirements.txt
```

### setup:

add a full GOOGLE_VISION_API_URL (with appended API key) to the file ```settings.py```

### how to run:

```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver

```


### unit tests:

```
python manage.py test
```